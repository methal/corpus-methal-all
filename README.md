Extraction de règles de variation orthographique en alsacien
===========

Travaux de **mémoire M2 de Heng Yang** sur l'extraction de règles de variation orthographique dans les dialectes alsaciens. 

- Le code développé pendant le stage se trouve dans le dossier [code](./code) et décrit [ci-dessous](#code-memoire)
- Le corpus exploité est un corpus de théâtre alsacien (1870-1940) développé dans le cadre du projet [MeThAL](https://methal.pages.unistra.fr/); le corpus est décrit [ci-dessous](#corpus)

<a id="code-memoire"></a>
# Code

Dans le dossier [code](./code), divisé selon les sous-dossiers suivants: `script`, `working_dir`, `Rstylo`, `aligne_variants_alsa`

## script

### text_extract

TEI -> dataframe initial

- **tei_reader.py** : Créer la classe (object) intégrant les données extraites des fichiers TEI
- **tei_to_dataframe.py**, **tei2_to_dataframe.py** :  traitement initial
  - Parsing TEI XML fichiers avec pandas.DataFrame, où les métadonnées sont extraites de ses tags de fichier XML, certaines données seront donc manquantes
  - **tei2** est différent des autres corpus, il est donc traité séparément ici
  - **tei_to_dataframe.py** pour corpus tei + tei-lustig, **tei2_to_dataframe.py** pour corpus tei2
  - Colonnes contenues dans le dataframe provisoire :  'IdMtl', 'FileName', 'Title', 'Author', 'Publisher', 'PubPlace', 'dateWritten', 'datePrint', 'Front', 'Body', 'textBrut'

Compléter les métadonnées et extraire le texte brut

- **extract_complete_tei.py**, **extract_complete_tei2.py** :  C'est à partir du claseur dans [autres/md](https://gitlab.huma-num.fr/methal/corpus-methal-all/-/tree/main/autres/md) que les métadonnées sont complétées. De plus, un fichier (.txt) **texte brut** de chaque pièce est extrait et stocké dans [working_dir/text_brut](https://gitlab.huma-num.fr/methal/corpus-methal-all/-/tree/main/code/working_dir/text_brut)
  - **extract_complete_tei.py** pour corpus tei + tei-lustig, **extract_complete_tei2.py** pour corpus tei2
  - Colonnes contenues dans le dataframe provisoire :  'FileName', 'Author', 'authorPlaceOfBirth',  'PubPlace', 'PubDept', 'datePrint'

### token

Tokenize et calcul du nombre de tokens

- **alsatian_tokeniser_multi.py** :  alsatian tokeniser de @dbernhard
- **token_count.py** :  calcul du nombre de tokens

### metaphone

Générer la clé métaphone pour chaque mot des fichiers .tok

- **metaphone_als.py** :  Based on metaphone.py

- **match_mp.py** :  Faites correspondre des mots avec au moins une clé métaphone identique
  - Complexité en temps ：O(n^2)
  - Entrée ：*working_dir/tokens/all*
  - Sortie ：*working_dir/metaphone_json/nlettres.json*

- **match_mp_revdict.py** : Même fonction que ci-dessus mais inverser les valeurs des clés du dictionnaire. C'est plus rapide, mais étant donné qu'il y a trois cas de correspondance métaphone, il ne peux faire que la correspondance forte ( key1 == key1) et la correspondance minimale ( key2 == key2)
  - Complexité en temps ：O(n)
  - Entrée ：*working_dir/tokens/all*
  - Sortie ：Pas sûr qu'il soit utile par la suite, car les résultats sont incomplets

- **visualize_forme_zeta.py** :  Faire diagramme à barres basé sur les différents scores(zeta) de forme
  - Entrée ：*working_dir/metaphone_json/6lettres.json* et mesures discriminativite par pydistinto
  - Sortie ：*working_dir/plot/metaphone_forme_zeta/ \*.svg*
  - Usage ：python visualize_forme_zeta.py -h

**Combine_CSVs.py** :  combiner les fichiers csv

## working_dir

### mesures_discriminativite

Documents originalement sur Seafile, transférés ici

- **output_quanteda_sans_TEI2** : keyness (khi2) et autres mesures (pas nécessairement de "discriminativité", il y a aussi des distributions de fréquences et un dendrogramme par pièce
- **pydistinto** : Dépôt pydistinto cloné avec modifs @hyang1, y compris des diagrammes discriminativité et dataframe dont les résultats sont visualisés

### metadata

- **temp** :  Fichiers de métadonnées provisoires générés par le script
- **metadata.csv** :  Métadonnées complètes actuelles, colonnes =>  'FileName', 'Author', 'authorPlaceOfBirth',  'PubPlace', 'PubDept', 'datePrint', 'period', 'Tokens', 'TokensNoPunctuation'

### text_brut

- **bas-rhin** :  Texte brut de chaque pièce (bas-rhin)
- **haut-rhin** :  Texte brut de chaque pièce (haut-rhin)

### tokens

- **all** :  Tokens de chaque pièce à partir du texte brut (bas-rhin et haut-rhin)
- **bas-rhin** :  Tokens de chaque pièce à partir du texte brut (bas-rhin)
- **haut-rhin** :  Tokens de chaque pièce à partir du texte brut (haut-rhin)

### metaphone

 La structure initiale, sans contexte

- **6lettres.json** : Toutes les correspondances des clés métaphone pour les mots d'une longueur supérieure ou égale à 6 lettres
- **3grams.csv**, **4grams.csv,** **5grams.csv**, **words.csv** : Toutes les correspondances des clés métaphone pour les ngrams ou les tokens 

### plot

- **metadata_ratio** : Diagrammes de distribution des pièces et tokens par période, auteur·e et région
- **metaphone_forme_zeta** : Diagrammes basé sur les différents scores(zeta) de forme avec une même clé métaphone



## Rstylo

### data4r

Texte tokénisé avec tokéniseur Delphine en entrée

### output_oppose()

Analyse contrastive entre deux ensembles de textes donnés. Elle génère une liste de mots significativement préférés par un auteur testé (ou, un ensemble d'corpus), et une autre liste contenant les mots significativement évités par le premier lorsqu'il est comparé à un autre ensemble de textes.

### stylo.r

Script pour la fonction "oppose()" dans *R stylo*

## aligne_variants_alsa

### bin

Scripts pour les algorithmes d'alignement et d'extraction de règles fournis par Alice Millour.

### data

Résultats obtenus par @hengy1 en traitant l'alsacien selon le script ci-dessus

[Diagramme de flux de travail](https://www.plantuml.com/plantuml/svg/jLRRZjis47tNLqpJXspGjPKuI9iuw2B1RbFqKZGFcw9j40XZQInpGv4AELnaK-HV-bZt3_hi7oj7aY-ClTssG0HV97B4ENFc38TFFAFZq6Z0ajMbugli4rvGGy3sAkiciwoDlugLDN2BwIr3_BfeVWj-opA8ryxy-LVVmEW4AcISEsm8b8apgCLMJ3COhYcJAXnXHMxSNZtUZ1lB19hcdC7ETUfMItAW4KgDtXCSsTa5bNm7bE7LidPe63HvgAmnqTGdRtTu5Sjq_CvLkL3aF4Ii6pCoLUSw1JctxdNtaA1cMspYss1Fy3jLBtWAhGqEIklQu859mMVoEzB1iweJ7u0rAhFdQVA_NKsImCJ53GIVTmGrP7YTiMD9grUc5fwsfE4bbN76LEZ7PyZuJA0T9slPrLzTBfhLad6T4F3MS989yF6FiVRmym_Vfoj8142UeLBp-Mhf19Qa3WCxyaESnw1qRsHD2R9kL0BurRB5g9O-b5BTaE7fN6c2gNWi6c8KjWk9iH2b5pCNU5pwovDz-gPmbefSA2l9CIgp8qJmv2Jo1isRW8R9fm3U3uzf6PtQfjMHFd87cAGrZqwGW00b4AHIP3VeToZTeXG7n5GmgO-duKFU9XzLnt1wMoIn-hrdqudEIeANBnVChP_cUMLBFwwjhJMD8wpSjx89-GKH-xpAxnLlNtntBpoVt7tqulJYsqTOlt75h-hrvm_2tOTF7xeVd_va7zn_ViuN3t_9gLByQQsgBx-uV-_HvC7ToMJOde9xzGfw9o0EKuWsxE2f5MkSZHUXmP49pNZkyePuWJhlMHZrJoFKEX_bx8Zo1fN9-rB8cmesTz7dcq3NbLuvI4f6nl-cAZnCOLCIJ4hgu3WPcNH36NnuRHNPqRz0WTMgrHybGON6Gp1GMUyLkPCisoc5B7xITcDVa-dxJ5fxvpAXtf4KGucw31gg40sQcIDnshteuGWlR91j0SFRb9GxsuYdg3soGbDmjwedLpH1ywOm4eMkZM5ZBFwUL3DoPe6kwfVi9gFkW-63groFkSULPA4Brj_CfT3dMwruhorBrjg5DNGhsI4IcwzNoyYCMxVPrL886ruRwhBttbXEb-clwSP9WThtKIVJoEMYQDfq9athCrJSR_2EV6jDrJ7ZnUNGUZ341zN5JNieLKM6LSIOUXxsUAE9sT7umbjpSXZ1dfRFRH10EW65fXpE4WB0VyZJtY9w7K5rMr1hQ-f0mO4FRIHSnLkIJI6UOr2Jx5q6UgYz6FwvLjzVcz1MMpwQMzVGw1qntZcOj4lb0-fOIKLdMOZb3HcId2Yi7JPDXtQ6Gapyyz9_Gz-ojftRWozjD3ZgG8YAnTE7ARAw8WTJs8feY-VaEZ1d0up_xhqNasIZMVtPx2df6ehbrhDjXpBRQNbJtXwWsgjAULQ6xUEqZbqKIz2CBEBPCJl93elmZFoMQRdrfLEDCjYTr2DWsjfkJzoLxS_2k-wyZ0sfzd4uI_JKXvGzYGEXqNy3)


<a id="corpus"></a>
# Corpus

Le corpus, provenant du projet [MeThAL](https://methal.pages.unistra.fr), est reproduit dans le dossier [pieces](./pieces) de ce dépôt

Chaque sous-dossier sous **pieces** contient les ressources suivantes :
- **tei** : les pièces encodées en TEI qui ont été publiées sur [Nakala](https://nakala.fr/collection/10.34847/nkl.feb4r8j9) et sur [GitLab](https://git.unistra.fr/methal/methal-sources) Unistra. La source première est une numérisation en mode image de ressources sur [Numistral](https://www.numistral.fr/services/engine/search/sru?operation=searchRetrieve&exactSearch=false&collapsing=true&version=1.2&query=(colnum%20adj%20%22BNUStr058%22)&suggest=10&keywords=), pour lesquelles nous avons effectué l'OCR, sa correction et l'encodage TEI
- **tei2** : pièces en TEI; versions de travail pas encore publiées sur un entrepôt de données par le projet, et sans DOI. Utilisables sachant que : 
  - Il y a eu moins de validation que pour les pièces publiées
  - Il y aura donc plus d'erreurs que dans ces dernières
  - Il manque de gérer les traits d'union en fin de ligne (donne un pourcentage de mots mal découpés)
- **tei-lustig** : pièces en TEI dont la source est des documents sur [Wikisource](https://als.wikipedia.org/wiki/Text:August_Lustig/A._Lustig_S%C3%A4mtliche_Werke:_Band_2), en format wiki-markup. Il s'agit des œuvres complètes d'August Lustig. Elles ont été transformées en TEI par script

## Métadonnées pour les pièces

- Le dossier [autres/md](./autres/md) contient un export du classeur contenant les métadonnées des pièces
	- La colonne `shortName` de l'onglet `pieces` correspond aux noms de fichiers utilisés dans les différents dossiers de ce dépôt. Pour les documents où l'ID de la pièce n'est pas mentionné (c.à.d. les HTML et les tei-lustig), cette colonne devrait servir à obtenir les métadonnées pour le document en croisant avec le nom du fichier. Autrement, avec l'ID de la pièce on peut croiser avec la colonne `id` de l'onglet `pieces`
