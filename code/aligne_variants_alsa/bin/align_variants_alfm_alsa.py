from __future__ import print_function
import codecs
import pandas as pd
import time, os, fnmatch, shutil
import os


# -------------------------------------------------------------------------------------------
# from a csv file of n variants, create one file per lemma aligning variants thanks to alfm
# -------------------------------------------------------------------------------------------

# REQUIREMENTS : - almf executable in the execution dir
#                - list of variants in a comma-separated file
# example : variant_list.csv
# 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,
# ommu,ommi,omi,omu,omini,ommini,
# capelli,capeddi,capiddi,
# ochju,ochji,ochja,

# OUTPUTS : for each line of variants : var1, var2, var3
# alpha_files/var1_alpha.txt
# alpha_files/var1_seq.txt
# alpha_files/align/var1_align.txt


# -------------------------------------------------------------------------------------------
# STEPS
# -------------------------------------------------------------------------------------------
# step 1. the alphabet must be mapped to C compatible strings to be processed by alfm

# step 2. create an "alpha" file that specifies the weights map of each possible substitution
# given the possible characters in each existing variant for a given lemma
#    - same letter : weight of 3 (must be aligned)
#    - vowel vowel / consonant consonant : weight of 2 (probably aligned)
#    - vowel gap / consonant gap : weight of 1 (may be aligned) gap = deletion
#    - vowel consonant : weight of 0 (unlikely aligned => creates deletion / insertion)
# example :  acu, lacu, agu
# generates : acu_alpha.txt
# 6  #nb of distinct characters
# a l c g u - #list of distinct characters.  Below : corresponding weights map
# 3
# 0 3
# 0 2 3
# 0 2 2 3
# 2 0 0 0 3
# 0 0 0 0 0 0

# step 3. create a "seq" file that lists variants
# example : acu_seq
# >0
# acu
# >1
# lacu
# >2
# agu


# step 4. create a "align" file by runing alfm script
# example : acu_align
# Num simbols =6
# A L C G U -
# (1,1)=3.000000
# (2,1)=0.000000(2,2)=3.000000
# (3,1)=0.000000(3,2)=2.000000(3,3)=3.000000
# (4,1)=0.000000(4,2)=2.000000(4,3)=2.000000(4,4)=3.000000
# (5,1)=2.000000(5,2)=0.000000(5,3)=0.000000(5,4)=0.000000(5,5)=3.000000
# (6,1)=0.000000(6,2)=0.000000(6,3)=0.000000(6,4)=0.000000(6,5)=0.000000(6,6)=0.000000
#
# ..
# .
#  Similaritat entre les sequencies
# [1,2]=9.000000,[1,3]=8.000000,
# [2,3]=8.000000,
# ..
# Number of sequnces=3  Alignment length=4  Alignment score=25
# 0          -ACU
# 1          LACU
# 2          -AGU
#             *
#
#
#  2.2.4

# -------------------------------------------------------------------------------------------
# VARIABLES
# -------------------------------------------------------------------------------------------


t = time.localtime()

timestamp = time.strftime('%b-%d-%Y', t)
df_bas = pd.read_csv("../data/Craig_bas_10.csv",dtype=str);
df_haut = pd.read_csv("../data/Craig_haut_10.csv",dtype=str);

OutputDir="../data/variants/" # Contains alpha_files and generated rules


# Map towards C compatible strings
map = {'à': '0', 'è': '1', 'ì': '2', 'ò': '3', 'ù': '4'
, 'á': '5', 'é': '6', 'í': '7', 'ó': '8', 'ú': '9'
, 'ä': '!', 'ë': 'ø', 'ï': '#', 'ö': '%', 'ü': '='
, 'â': '[', 'ê': '(', 'î': ')', 'ô': '*', 'û': '+', 'ß': '{', '\'': '~', '\'': '~', '-':']', 'æ':'}'}


# inverse Map from C compatible strings
inv_map = {v: k for k, v in map.items()}


# -------------------------------------------------------------------------------------------
# UTILS
# -------------------------------------------------------------------------------------------


def voyelle(lettre):
    a = ['a', 'e', 'i', 'o', 'u', 'y','0', '1', '2', '3', '4'
    , '5', '6', '7', '8', '9'
    , '!', '[', '#', '%', '=', '}'
    , '\'', '(', ')', '*', '+']
    return(lettre in a)

def dentale(lettre):
    a = ['t','d']
    return(lettre in a)

def gap(lettre):
    return(lettre=='-')

def labiale(lettre):
    a = ['p','b']
    return(lettre in a)


# -------------------------------------------------------------------------------------------
# CODE
# -------------------------------------------------------------------------------------------


#### Step 1: map symbols
def map_symbols(df):
    for i in df.columns[0:]:
        print(df[i])
        for j in df.index[0:]:
            if type(df[i][j]) is str:

                df[i][j]=df[i][j].lower();
                pos=0
                for char in df[i][j]:
                    if char in map.keys():
                        s = list(df[i][j])
                        s[pos]=map[char]
                        df[i][j]="".join(s)
                    pos=pos+1;

map_symbols(df_bas)
map_symbols(df_haut)


#### Step 2: Generate alpha file for each variant of bas-rhin with the other variants of haut-rhin (they share the same metaphone key)

for i in df_bas.index[0:]:
    # loop through list of variants lists
    tot_string=""
    # print(i)
    for j in df_bas.columns[0:]:
        # print(j)
        # loop through line : list of variants
        if type(df_bas[j][i]) is str:
            tot_string=df_bas[j][i]
            # print(tot_string) # the one variant in bas-rhin for comparison
        else:
            '''
            The loop will iterate over the maximum number of columns in the dataframe,
            but some rows(lines) do not have that many columns. So when the first null element is reached in each row,
            the loop is skipped and moves on to the next round
            '''
            continue

        # variants of haut-rhin that share the same key with that variant of bas-rhin (variable: df_bas[j][i])
        # the line with the same metaphone key as in df_bas
        for k in df_haut.index[i:i+1]:
            # print(k)
            # loop through list of variants lists

            for q in df_haut.columns[0:]:
                # loop through line : list of variants
                if type(df_haut[q][k]) is str:
                    tot_string=tot_string+df_haut[q][k]
                    # print(tot_string)

                # list of possible characters for a given word
                charlist=''.join(set(tot_string))

            # create alpha file
            filename=OutputDir+"alpha_files/"+df_bas[j][i]+"_alpha.txt"
            # print(filename)

            with codecs.open(filename, "w",'utf-8') as f:
                # alpha file 1st line
                f.write(str(len(charlist)+1))
                f.write("\n")

                # alpha file 2nd line
                for char in charlist:
                    f.write(char+" ")
                f.write("-")
                f.write("\n")

                # alpha file weight map
                char_count=len(charlist)
                for n in range(char_count):
                    for m in range(n+1):
                        charlist[n]
                        charlist[m]
                        if n==m:
                            f.write("4 ")
                        else:
                            if voyelle(charlist[n]) & voyelle(charlist[m]):
                                f.write("3 ")
                            elif (not voyelle(charlist[n])) & (not voyelle(charlist[m])):
                                f.write("3 ")
                            else:
                                f.write("2 ")
                    f.write("\n")
                # alpha file final line (gap symbol)
                for n in range(char_count+1):
                    f.write("1 ")
                f.write("\n")


#### Step 3: Generate seq file for each variant of bas-rhin with the other variants of haut-rhin (they share the same metaphone key)

for i in df_bas.index[0:]:
    for j in df_bas.columns[0:]:
        if type(df_bas[j][i]) is str:
            print("\n"+df_bas[j][i])
            filename_seq=OutputDir+"/alpha_files/"+df_bas[j][i]+"_seq.txt"
        else:
            continue
        # print(filename_seq)
        with codecs.open(filename_seq, "w",'utf-8') as f:
            # print(">" + "0" + "\n" + df_bas[j][i] + "\n" )
            print(df_bas[j][i])
            f.write(">" + "0" + "\n" + df_bas[j][i] + "\n" )
            for k in df_haut.index[i:i+1]:
                for q in df_haut.columns[0:]:
                    if type(df_haut[q][k]) is str:
                        print(df_haut[q][k])
                        num = int(q) + 1
                        # print(">" + str(num) + "\n" + df_haut[q][k] + "\n" )
                        f.write(">" + str(num) + "\n" + df_haut[q][k] + "\n" )


#### Step 4: Generates align file (run alfm script):



def path_remake(path):
    # il peut y avoir des caractères ' ','(',')' (**espaces, parenthèses de gauche, parenthèses de droite respectivement**) dans les chemins linux.
    return path.replace(' ', '\ ').replace('(','\(').replace(')','\)')



for i in df_bas.index[0:]:
    for j in df_bas.columns[0:]:
        if type(df_bas[j][i]) is str:
            print("\n"+df_bas[j][i])
            filename_alpha=OutputDir+"alpha_files/"+df_bas[j][i]+"_alpha.txt"
            filename_seq=OutputDir+"/alpha_files/"+df_bas[j][i]+"_seq.txt"
            # print(filename_alpha,filename_seq)

            filename_align = OutputDir+"/alpha_files/align/" + df_bas[j][i] + "_align.txt"

            filename_alpha = path_remake(filename_alpha)
            filename_seq = path_remake(filename_seq)
            filename_align = path_remake(filename_align)

            os.system("./alfm "+filename_alpha+" "+filename_seq+" > "+ filename_align)