# coding=utf-8

from __future__ import print_function
import pandas as pd
import time
import difflib
import re
import itertools
import numpy as np



# -------------------------------------------------------------------------------------------
# From a list of variants and corresponding alignments, creates a file of rules
# -------------------------------------------------------------------------------------------

# REQUIREMENTS : - almf executable in the execution dir
#                - list of variants in a comma-separated file
# example : variant_list.csv
# 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,
# ommu,ommi,omi,omu,omini,ommini,
# capelli,capeddi,capiddi,
# ochju,ochji,ochja,
#
#                - list of generated alignements (see aligh_variants_alfm.py)
# example : ../data/alpha_files/acu_align.txt
# Num simbols =6
# G C L A U -
# (1,1)=3.000000
# (2,1)=2.000000(2,2)=3.000000
# (3,1)=2.000000(3,2)=2.000000(3,3)=3.000000
# (4,1)=0.000000(4,2)=0.000000(4,3)=0.000000(4,4)=3.000000
# (5,1)=0.000000(5,2)=0.000000(5,3)=0.000000(5,4)=2.000000(5,5)=3.000000
# (6,1)=1.000000(6,2)=1.000000(6,3)=1.000000(6,4)=1.000000(6,5)=1.000000(6,6)=1.000000
#
# ..
# .
#  Similaritat entre les sequencies
# [1,2]=10.000000,[1,3]=8.000000,
# [2,3]=9.000000,
# ..
# Number of sequnces=3  Alignment length=5  Alignment score=28
# 0          -A-CU
# 1          LA-CU
# 2          -AG-U
#             *
#
#
#  2.2.4


# OUTPUTS :
# variants_rules/rules_strict_auto.txt (generated in C compatible alphabet and
# ordered by descending frequency) :
# ^R,^GR,68
# ^GR,^R,68
# NE$,NU$,64
# NU$,NE$,64
# NI$,NE$,45

# variants_rules/rules_strict_auto.txt.readable (ordered by descending frequency)
# 0	^r	^gr	68
# 1	^gr	^r	68
# 2	ne$	nu$	64
# 3	nu$	ne$	64
# 4	ni$	ne$	45

# -------------------------------------------------------------------------------------------
# STEPS
# -------------------------------------------------------------------------------------------
# step 1. the alphabet must be mapped to C compatible strings to be processed by alfm

# step 2. generate rule file by looping through variants and corresponding alignment files.



# -------------------------------------------------------------------------------------------
# VARIABLES
# -------------------------------------------------------------------------------------------

t = time.localtime()
timestamp = time.strftime('%b-%d-%Y', t)


# Map towards C compatible strings
map = {'à': '0', 'è': '1', 'ì': '2', 'ò': '3', 'ù': '4'
, 'á': '5', 'é': '6', 'í': '7', 'ó': '8', 'ú': '9'
, 'ä': '!', 'ë': 'ø', 'ï': '#', 'ö': '%', 'ü': '='
, 'â': '[', 'ê': '(', 'î': ')', 'ô': '*', 'û': '+', 'ß': '{', '\'': '~', '\'': '~', '-':']', 'æ':'}'}

# inverse Map from C compatible strings
inv_map = {v: k for k, v in map.items()}

# df = pd.read_csv("../data/variant_list.csv", ",");
df_bas = pd.read_csv("../data/Craig_bas_10.csv",',');
# df_haut = pd.read_csv("../data/haut_variant_list.csv",dtype=str);


# -------------------------------------------------------------------------------------------
# UTILS
# -------------------------------------------------------------------------------------------

def auto_rules_to_readable(rules_file):
    # Transform automatically generated rules (eg. rules_strict_auto.txt)
    # to readable rules (eg. rules_strict_auto.txt.readable)
    # thanks to inverse mapping

    df = pd.read_csv(rules_file, "\t");

    for i in df.columns[0:]:
        for j in df.index[0:]:
            if type(df[i][j]) is str:
                df[i][j] = df[i][j].lower()
                pos = 0
                for char in df[i][j]:
                    if char in inv_map.keys():
                        s = list(df[i][j])
                        s[pos] = inv_map[char]
                        df[i][j] = "".join(s)
                    pos = pos + 1;

    grouped = df.groupby(['var1', 'var2'], as_index=False).size().sort_values(ascending=False,by="size").reset_index()
    grouped.to_csv(rules_file + ".readable", sep='\t')
    df = grouped
    print("The rules that will apply are the following:")
    print(df)

# pair variants (le premier élément et les autres éléments)
def combinations_pair(iterable):

    pool = tuple(iterable)
    n = len(pool)

    indices = list(range(n))
    # print(indices)

    for num in indices[1:]:
        indices = [0,num]
        # print(indices)
        yield tuple(pool[i] for i in indices)


def get_strict_rules(rulefile):
    # strict rules force
    # LEFT AND RIGHT contexts to be equal :
    # ex: NE$,NU$
    # counter example: NE$, NUA (only left context is equal here)

    with open(rulefile+".tmp", 'w') as fr:
        # 1st line
        fr.write("var1\tvar2\n")

        # loop through mapped df of variants

        for i in df_bas.index[0:]:
            for j in df_bas.columns[0:]:
                if type(df_bas[j][i]) is str:
                    print("\n"+df_bas[j][i])
                    filename_align = "../data/variants/alpha_files/align/" + df_bas[j][i] + "_align.txt"
                    f = open(filename_align, "r", encoding = "ISO-8859-1")
                    contents = f.read()
                    print(contents)


            with open(filename_align, encoding = "ISO-8859-1") as fp:
                array = []
                for result in re.findall('Number of sequnces=[0-9]+  Alignment length=[0-9]+  Alignment score=[0-9]+(.*?)$', fp.read(), re.S):
                    for match in re.findall('[0-9]+ *([^\.].*?)$', result, re.MULTILINE):
                        # array.append("^"+match+"$")
                        array.append(match)

                print("array=", array)


                # we infer the rules for each word's variant list
                array_unique=np.unique(array)
                print("arrayunique=", array_unique)

                for pair in combinations_pair(array_unique):
                    s1=pair[0]
                    s2=pair[1]
                    print("\ns1=",s1,"\ns2=",s2)
                    start = 0
                    while start <= len(s1)-2:
                        no_rule=False
                        while start <= len(s1)-1 and s1[start] == s2[start] :
                            start+=1
                        # difference has been found
                        print("start : ", start)
                        print(len(s1)-1)
                        print(s1[start-1], s2[start-1])
                        if start==0:
                            print("difference on first char")
                            # difference on first char => ignored
                            pass;
                        elif start == len(s1):
                            print("reached eow")
                            # reached end of word => ignored
                            pass;
                        elif start == len(s1)-1:
                            print("difference on last char")
                            # difference en last char => ignored
                            pass;
                        # elif s1[start-1] == "-":
                        #
                        #     print("left context is blank")
                        #     # left context is blank => ignored
                        #     pass;
                        elif s1[start-1] == s2[start-1] :

                            if s1[start - 1] == "-":
                                if s1[start-2] == s2[start-2] and s1[start - 2] != "-":
                                    real_start=start - 2
                                else:
                                    no_rule=True
                            else :
                                real_start = start - 1
                            # left context is equal => look for right context

                            if not no_rule:

                                print("left context equal :", s1[start-1])
                                end = start+1

                                while end < len(s1)-1 and s1[end] != s2[end]  :
                                    end+=1

                                if end == len(s1)-1 and s1[end] != '_' :
                                    # we did not find right context
                                    pass;
                                else :
                                    # we found right context
                                    if s1[end]== "-":
                                        pass;
                                    else :
                                        print(s1[start-1:end + 1])
                                        print(s2[start-1:end + 1])
                                        fr.write(s1[real_start:end + 1].replace("-", "") + "\t" + s2[real_start:end + 1].replace("-",
                                                                                                                    "") + "\n")
                                if end < len(s1)-1 :
                                    start = end
                                    print("new_start : ", start)
                                else :
                                    print("new_start : ", start)
                                    start = len(s1)-1
                        start+=1


    df_rules = pd.read_csv(rulefile+".tmp", "\t");
    df_rules = df_rules.sort_values('var1')
    df_rules.to_csv(rulefile, sep='\t')
    auto_rules_to_readable(rulefile)

    grouped=df_rules.groupby(df_rules.columns.tolist(),as_index=False).size().sort_values(ascending=False, by="size")
    grouped.to_csv(rulefile+".stats", header=False)

    grouped=df_rules.groupby(df_rules.columns.tolist(),as_index=False).size()
    print(grouped.sort_values(ascending=False, by="size"))

# -------------------------------------------------------------------------------------------
# CODE
# -------------------------------------------------------------------------------------------

#### Step 1: map symbols (variant_list.csv content)


def map_symbols(df):
    for i in df.columns[0:]:
        print(df[i])
        for j in df.index[0:]:
            if type(df[i][j]) is str:

                df[i][j]=df[i][j].lower();
                pos=0
                for char in df[i][j]:
                    if char in map.keys():
                        s = list(df[i][j])
                        s[pos]=map[char]
                        df[i][j]="".join(s)
                    pos=pos+1;
map_symbols(df_bas)




#### Step 2: loop through variant list:
#### For each variant : - check alignments file
####                    - create/update substitution rules

StrictRuleFile="../data/variants/rules/rules_strict_auto.txt"

get_strict_rules(StrictRuleFile)
