## script

### text_extract

TEI -> dataframe initial

- **tei_reader.py** : Créer la classe (object) intégrant les données extraites des fichiers TEI
- **tei_to_dataframe.py**, **tei2_to_dataframe.py** :  traitement initial
  - Parsing TEI XML fichiers avec pandas.DataFrame, où les métadonnées sont extraites de ses tags de fichier XML, certaines données seront donc manquantes
  - **tei2** est différent des autres corpus, il est donc traité séparément ici
  - **tei_to_dataframe.py** pour corpus tei + tei-lustig, **tei2_to_dataframe.py** pour corpus tei2
  - Colonnes contenues dans le dataframe provisoire :  'IdMtl', 'FileName', 'Title', 'Author', 'Publisher', 'PubPlace', 'dateWritten', 'datePrint', 'Front', 'Body', 'textBrut'

Compléter les métadonnées et extraire le texte brut

- **extract_complete_tei.py**, **extract_complete_tei2.py** :  C'est à partir de ce [classeur](https://docs.google.com/spreadsheets/d/1_xUK1uP209UCjJ9agqr_Zik65u08A8rOAVo53PTtj8Y/edit#gid=731925022) google docs ou [autres/md ](https://gitlab.huma-num.fr/methal/corpus-methal-all/-/tree/main/autres/md)que les métadonnées sont complétées. De plus, un fichier (.txt) **texte brut** de chaque pièce est extrait et stocké dans [working_dir/text_brut](https://gitlab.huma-num.fr/methal/corpus-methal-all/-/tree/main/code/working_dir/text_brut)
  - **extract_complete_tei.py** pour corpus tei + tei-lustig, **extract_complete_tei2.py** pour corpus tei2
  - Colonnes contenues dans le dataframe provisoire :  'FileName', 'Author', 'authorPlaceOfBirth',  'PubPlace', 'PubDept', 'datePrint'

### token

Tokenize et calcul du nombre de tokens

- **alsatian_tokeniser_multi.py** :  alsatian tokeniser de @dbernhard
- **token_count.py** :  calcul du nombre de tokens

### metaphone

Générer la clé métaphone pour chaque mot des fichiers .tok

- **metaphone_als.py** :  Based on metaphone.py

- **match_mp.py** :  Faites correspondre des mots avec au moins une clé métaphone identique
  - Complexité en temps ：O(n^2)
  - Entrée ：*working_dir/tokens/all*
  - Sortie ：*working_dir/metaphone_json/nlettres.json*

- **match_mp_revdict.py** : Même fonction que ci-dessus mais inverser les valeurs des clés du dictionnaire. C'est plus rapide, mais étant donné qu'il y a trois cas de correspondance métaphone, il ne peux faire que la correspondance forte ( key1 == key1) et la correspondance minimale ( key2 == key2)
  - Complexité en temps ：O(n)
  - Entrée ：*working_dir/tokens/all*
  - Sortie ：Pas sûr qu'il soit utile par la suite, car les résultats sont incomplets

- **visualize_forme_zeta.py** :  Faire diagramme à barres basé sur les différents scores(zeta) de forme
  - Entrée ：*working_dir/metaphone_json/6lettres.json* et mesures discriminativite par pydistinto
  - Sortie ：*working_dir/plot/metaphone_forme_zeta/ \*.svg*
  - Usage ：python visualize_forme_zeta.py -h

**Combine_CSVs.py** :  combiner les fichiers csv

## working_dir

### mesures_discriminativite

Documents originalement sur Seafile, transférés ici

- **output_quanteda_sans_TEI2** : keyness (khi2) et autres mesures (pas nécessairement de "discriminativité", il y a aussi des distributions de fréquences et un dendrogramme par pièce
- **pydistinto** : Dépôt pydistinto cloné avec modifs @hyang1, y compris des diagrammes discriminativité et dataframe dont les résultats sont visualisés

### metadata

- **temp** :  Fichiers de métadonnées provisoires générés par le script
- **metadata.csv** :  Métadonnées complètes actuelles, colonnes =>  'FileName', 'Author', 'authorPlaceOfBirth',  'PubPlace', 'PubDept', 'datePrint', 'period', 'Tokens', 'TokensNoPunctuation'

### text_brut

- **bas-rhin** :  Texte brut de chaque pièce (bas-rhin)
- **haut-rhin** :  Texte brut de chaque pièce (haut-rhin)

### tokens

- **all** :  Tokens de chaque pièce à partir du texte brut (bas-rhin et haut-rhin)
- **bas-rhin** :  Tokens de chaque pièce à partir du texte brut (bas-rhin)
- **haut-rhin** :  Tokens de chaque pièce à partir du texte brut (haut-rhin)

### metaphone

 La structure initiale, sans contexte

- **6lettres.json** : Toutes les correspondances des clés métaphone pour les mots d'une longueur supérieure ou égale à 6 lettres
- **3grams.csv**, **4grams.csv,** **5grams.csv**, **words.csv** : Toutes les correspondances des clés métaphone pour les ngrams ou les tokens 

### plot

- **metadata_ratio** : Diagrammes de distribution des pièces et tokens par période, auteur·e et région
- **metaphone_forme_zeta** : Diagrammes basé sur les différents scores(zeta) de forme avec une même clé métaphone



## Rstylo

### data4r

Texte tokénisé avec tokéniseur Delphine en entrée

### output_oppose()

Analyse contrastive entre deux ensembles de textes donnés. Elle génère une liste de mots significativement préférés par un auteur testé (ou, un ensemble d'corpus), et une autre liste contenant les mots significativement évités par le premier lorsqu'il est comparé à un autre ensemble de textes.

### stylo.r

Script pour la fonction "oppose()" dans *R stylo*

## aligne_variants_alsa

### bin

Scripts pour les algorithmes d'alignement et d'extraction de règles fournis par @alice.

### data

Résultats obtenus par @hengy1 en traitant l'alsacien selon le script ci-dessus

[Diagramme de flux de travail](https://www.plantuml.com/plantuml/svg/jLRRZjis47tNLqpJXspGjPKuI9iuw2B1RbFqKZGFcw9j40XZQInpGv4AELnaK-HV-bZt3_hi7oj7aY-ClTssG0HV97B4ENFc38TFFAFZq6Z0ajMbugli4rvGGy3sAkiciwoDlugLDN2BwIr3_BfeVWj-opA8ryxy-LVVmEW4AcISEsm8b8apgCLMJ3COhYcJAXnXHMxSNZtUZ1lB19hcdC7ETUfMItAW4KgDtXCSsTa5bNm7bE7LidPe63HvgAmnqTGdRtTu5Sjq_CvLkL3aF4Ii6pCoLUSw1JctxdNtaA1cMspYss1Fy3jLBtWAhGqEIklQu859mMVoEzB1iweJ7u0rAhFdQVA_NKsImCJ53GIVTmGrP7YTiMD9grUc5fwsfE4bbN76LEZ7PyZuJA0T9slPrLzTBfhLad6T4F3MS989yF6FiVRmym_Vfoj8142UeLBp-Mhf19Qa3WCxyaESnw1qRsHD2R9kL0BurRB5g9O-b5BTaE7fN6c2gNWi6c8KjWk9iH2b5pCNU5pwovDz-gPmbefSA2l9CIgp8qJmv2Jo1isRW8R9fm3U3uzf6PtQfjMHFd87cAGrZqwGW00b4AHIP3VeToZTeXG7n5GmgO-duKFU9XzLnt1wMoIn-hrdqudEIeANBnVChP_cUMLBFwwjhJMD8wpSjx89-GKH-xpAxnLlNtntBpoVt7tqulJYsqTOlt75h-hrvm_2tOTF7xeVd_va7zn_ViuN3t_9gLByQQsgBx-uV-_HvC7ToMJOde9xzGfw9o0EKuWsxE2f5MkSZHUXmP49pNZkyePuWJhlMHZrJoFKEX_bx8Zo1fN9-rB8cmesTz7dcq3NbLuvI4f6nl-cAZnCOLCIJ4hgu3WPcNH36NnuRHNPqRz0WTMgrHybGON6Gp1GMUyLkPCisoc5B7xITcDVa-dxJ5fxvpAXtf4KGucw31gg40sQcIDnshteuGWlR91j0SFRb9GxsuYdg3soGbDmjwedLpH1ywOm4eMkZM5ZBFwUL3DoPe6kwfVi9gFkW-63groFkSULPA4Brj_CfT3dMwruhorBrjg5DNGhsI4IcwzNoyYCMxVPrL886ruRwhBttbXEb-clwSP9WThtKIVJoEMYQDfq9athCrJSR_2EV6jDrJ7ZnUNGUZ341zN5JNieLKM6LSIOUXxsUAE9sT7umbjpSXZ1dfRFRH10EW65fXpE4WB0VyZJtY9w7K5rMr1hQ-f0mO4FRIHSnLkIJI6UOr2Jx5q6UgYz6FwvLjzVcz1MMpwQMzVGw1qntZcOj4lb0-fOIKLdMOZb3HcId2Yi7JPDXtQ6Gapyyz9_Gz-ojftRWozjD3ZgG8YAnTE7ARAw8WTJs8feY-VaEZ1d0up_xhqNasIZMVtPx2df6ehbrhDjXpBRQNbJtXwWsgjAULQ6xUEqZbqKIz2CBEBPCJl93elmZFoMQRdrfLEDCjYTr2DWsjfkJzoLxS_2k-wyZ0sfzd4uI_JKXvGzYGEXqNy3)

