## Question : comment exploiter ces sorties ?

P. ex. pour extraire des règles de variation ou au moins pour examiner des patrons de variation 

P-ê des tableaux comme suit ? :

|mesure|BasRhinClés|BasRhinPreferredNgram|BasRhinAvoidedNgram|
|---|---|---|---|
|CraigZeta|clé_1|ngram_original|ngram_original|
|CraigZeta|clé_1|ngram_original|ngram_original|
|CraigZeta|clé_2|ngram_original|ngram_original|
|CraigZeta|clé_2|ngram_original|ngram_original|
|EderZeta|clé_3|ngram_original|ngram_original|
|EderZeta|clé_3|ngram_original|ngram_original|
|Khi2|clé_4|ngram_original|ngram_original|



n_gram_original c.à.d avec les tirets et les espaces
