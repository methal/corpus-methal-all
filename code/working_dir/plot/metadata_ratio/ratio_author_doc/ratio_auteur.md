## Haut-Rhin

```R
--- Haut-Rhin --- 

              Author Count   Prop 
--------------------------------- 
       August Lustig   26   0.667 
CharlesFrédéricKttnr    1   0.026 
       Charles Weber    1   0.026 
        Emile Wagner    1   0.026 
      Fernand Kuehne    1   0.026 
    Madeleine Weigel    1   0.026 
         Martin Huck    1   0.026 
        Paul Clemens    6   0.154 
     Xavier Sengelin    1   0.026 
--------------------------------- 
               Total   39   1.000 

```

![](https://seafile.unistra.fr/lib/1d3dd7ef-a48e-46b2-b2bd-a437803d3d4e/file/output/metadata_ratio_plots/ratio_author/Haut-Rhin_Author1.png?raw=1)



## Bas-Rhin

```R
--- Bas-Rhin --- 

              Author Count   Prop 
--------------------------------- 
      Adolphe Horsch    2   0.053 
        Camille Jost    5   0.132 
         Emile Weber    3   0.079 
         Emilie Hahn    1   0.026 
         Ernst Fuchs    1   0.026 
   Ferdinand Bastian    6   0.158 
        Gustav Lasch    1   0.026 
    Gustave Stoskopf    6   0.158 
       H. Kleisecker    1   0.026 
 Heinrich Schneegans    1   0.026 
     Hermann Günther    1   0.026 
           Jean Riff    4   0.105 
JohannGeorgDanlArnld    1   0.026 
       Julius Greber    4   0.105 
          Marie Hart    1   0.026 
--------------------------------- 
               Total   38   1.000 

```

![](https://seafile.unistra.fr/lib/1d3dd7ef-a48e-46b2-b2bd-a437803d3d4e/file/output/metadata_ratio_plots/ratio_author/Bas-Rhin_Author1.png?raw=1)
