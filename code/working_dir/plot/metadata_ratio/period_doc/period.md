## Number of documents grouped by different period

```R
--- period --- 

               Before German era  French era  German era    Total 
Frequencies:                   1          33          43       77 
Proportions:               0.013       0.429       0.558    1.000 

```

## Number of documents by period and geography

![](https://seafile.unistra.fr/lib/1d3dd7ef-a48e-46b2-b2bd-a437803d3d4e/file/output/metadata_ratio_plots/period/period_place.png?raw=1)


